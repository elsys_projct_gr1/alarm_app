import {
    createConnection,
    subscribeEntities,
    callService,
    createLongLivedTokenAuth,
} from "../node_modules/home-assistant-js-websocket/dist/index.js";

(async () => {

    var hassUrl = "";
    var hass_access_token = "";

    // Read from secrets file and wait for it to finish
    await fetch("../secrets.json")
        .then(response => response.json())
        .then((json) => {
            hassUrl = json.hassUrl;
            hass_access_token = json.hass_access_token;
        })

    const auth = createLongLivedTokenAuth(
        hassUrl,
        hass_access_token,
    );

    var valid = false;
    var connection;
    while (!valid) {
        try {
            connection = await createConnection({ auth });
        } catch (err) {
            console.log("Error found: %s", err);
            continue;
        }
        valid = true;
    }

    subscribeEntities(connection, (entities) =>
        handle_entities(connection, entities));

})();


function handle_entities(connection, entities) {
    console.log(entities);

    // Only support for one room for now.
    // Have to see what the output looks like when 
    // multiple end devices are connected
    var rooms = [];
    rooms.push(document.getElementById('rom1'));
    rooms.push(document.getElementById('rom2'));
    rooms.push(document.getElementById('rom3'));
    rooms.push(document.getElementById('rom4'));
    rooms.push(document.getElementById('rom5'));
    // rooms.push(document.getElementById('rom6'));
    // rooms.push(document.getElementById('rom7'));
    // rooms.push(document.getElementById('rom8'));
    // rooms.push(document.getElementById('rom9'));
    // rooms.push(document.getElementById('rom10'));
    // rooms.push(document.getElementById('rom11'));
    // rooms.push(document.getElementById('rom12'));


    // This code is currently very dependent on the naming of the switches
    Object.keys(entities)
        .sort()
        .forEach((entId) => {
            if (entId.startsWith("light.")) {

                var switch_name = entId.split('.').pop();

                // get last value of switch name (should be same as room number)
                var room_num = parseInt(switch_name.slice(-1));

                if (switch_name.startsWith("ekgang_alarm_switch")) {
                    console.log("Toggle alarm room %d", room_num);
                    //entities[entId].state === "on" ? rooms[room_num - 1].style.fill = 'red' : rooms[room_num - 1].style.fill = '#e5e5e5';
                    entities[entId].state === "on" ? rooms[room_num - 1].style.fill = 'red' : null;

                }
                else if (switch_name.startsWith("ekgang_service_switch")) {
                    if (rooms[room_num - 1].style.fill !== 'red') {
                        console.log("Toggle service room %d", room_num);
                        //entities[entId].state === "on" ? rooms[room_num - 1].style.fill = 'orange' : rooms[room_num - 1].style.fill = '#e5e5e5';
                        entities[entId].state === "on" ? rooms[room_num - 1].style.fill = 'orange' : null;
                    }
                }
            }
        });

    rom1.onclick = function () {
        console.log('rom1 clicked')
        // callService(connection, 'light', 'turn_off', undefined, { entity_id: "light.ekgang_alarm_switch1" });
        // callService(connection, 'light', 'turn_off', undefined, { entity_id: "light.ekgang_service_switch1" });
        rom1.style.fill = '#e5e5e5';
    }

    rom2.onclick = function () {
        console.log('rom2 clicked')
        // callService(connection, 'light', 'turn_off', undefined, { entity_id: "light.ekgang_alarm_switch2" });
        // callService(connection, 'light', 'turn_off', undefined, { entity_id: "light.ekgang_service_switch2" });
        rom2.style.fill = '#e5e5e5';
    }

    rom3.onclick = function () {
        console.log('rom3 clicked')
        // callService(connection, 'light', 'turn_off', undefined, { entity_id: "light.ekgang_alarm_switch3" });
        // callService(connection, 'light', 'turn_off', undefined, { entity_id: "light.ekgang_service_switch3" });
        rom3.style.fill = '#e5e5e5';
    }

    rom4.onclick = function () {
        console.log('rom4 clicked')
        // callService(connection, 'light', 'turn_off', undefined, { entity_id: "light.ekgang_alarm_switch4" });
        // callService(connection, 'light', 'turn_off', undefined, { entity_id: "light.ekgang_service_switch4" });
        rom4.style.fill = '#e5e5e5';
    }

    rom5.onclick = function () {
        console.log('rom5 clicked')
        // callService(connection, 'light', 'turn_off', undefined, { entity_id: "light.ekgang_alarm_switch5" });
        // callService(connection, 'light', 'turn_off', undefined, { entity_id: "light.ekgang_service_switch5" });
        rom5.style.fill = '#e5e5e5';
    }

    rom6.onclick = function () {
        console.log('rom6 clicked')
        // callService(connection, 'light', 'turn_off', undefined, { entity_id: "light.ekgang_alarm_switch6" });
        // callService(connection, 'light', 'turn_off', undefined, { entity_id: "light.ekgang_service_switch6" });
        rom6.style.fill = '#e5e5e5';
    }

    rom10.onclick = function () {
        console.log('rom10 clicked')
        // callService(connection, 'light', 'turn_off', undefined, { entity_id: "light.ekgang_alarm_switch10" });
        // callService(connection, 'light', 'turn_off', undefined, { entity_id: "light.ekgang_service_switch10" });
        rom10.style.fill = '#e5e5e5';
    }

    rom11.onclick = function () {
        console.log('rom11 clicked')
        // callService(connection, 'light', 'turn_off', undefined, { entity_id: "light.ekgang_alarm_switch11" });
        // callService(connection, 'light', 'turn_off', undefined, { entity_id: "light.ekgang_service_switch11" });
        rom11.style.fill = '#e5e5e5';
    }

}
