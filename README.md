# README

## Setup

- Setup home assistant on a raspberry pi.
- Configure names of endpoints either on the home assistant or in `websocket.js`
- Add secrets to `secrets.json`
    - `hassUrl` is the url to the home assistant
    - `hass_access_token` is a "Long-Lived Access Tokens" found under Profile -> Security.

## Autorun application on boot

1. Make sure that paths and user in `alarmsystem.service` is correct.
2. Copy file to `/etc/systemd/system` by running the following command:
```bash
sudo cp alarmsystem.service /etc/systemd/system
```
3. Enable the alarmsystem script to run on boot:
```bash
systemctl enable alarmsystem.service
```